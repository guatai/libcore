package com.suv.libcore.secure;

public final class SecureKit {
	public static String byteToHex(byte[] data) {
		String tmp = null;
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			tmp = Integer.toHexString(data[i] & 0xFF);
			if (tmp.length() == 1) {
				str.append(0).append(tmp);
			} else {
				str.append(tmp);
			}
		}
		return str.toString();
	}

	public static byte[] hexToByte(String hexStr) {
		int len = hexStr.length() / 2;
		char[] chrs = hexStr.toCharArray();
		byte[] ret = new byte[len];
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			ret[i] = (byte) (charToByte(chrs[pos]) << 4 | charToByte(chrs[pos + 1]));
		}
		return ret;
	}

	private static byte charToByte(char c) {
		return (byte) "0123456789abcdef".indexOf(c);
	}
}
