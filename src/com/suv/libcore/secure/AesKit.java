package com.suv.libcore.secure;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.suv.libcore.util.StrKit;

public final class AesKit {
	public static final String ALGORITHM = "AES";

	public static byte[] encrypt(String content, String password, String charset, int keysize) throws NoSuchAlgorithmException,
			UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
			NoSuchProviderException {
		return encrypt(content.getBytes(charset), password, charset, keysize);
	}

	public static byte[] decrypt(String content, String password, String charset, int keysize) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
			NoSuchProviderException {
		return decrypt(content.getBytes(charset), password, charset, keysize);
	}

	public static byte[] encrypt(byte[] content, String password, String charset, int keysize) throws NoSuchAlgorithmException,
			UnsupportedEncodingException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
			NoSuchProviderException {
		KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM);
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", "Crypto");
		secureRandom.setSeed(password.getBytes(charset));
		kg.init(keysize, secureRandom);
		SecretKey secretKey = kg.generateKey();
		byte[] encrypt_data = secretKey.getEncoded();
		SecretKeySpec sks = new SecretKeySpec(encrypt_data, ALGORITHM);
		Cipher cipher = Cipher.getInstance("AES/ECB/ZeroBytePadding");// 解决android4.2.2上pad
																		// block
																		// corrupted
		cipher.init(Cipher.ENCRYPT_MODE, sks);
		byte[] result = cipher.doFinal(content);
		return result;
	}

	public static byte[] decrypt(byte[] content, String password, String charset, int keysize) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException,
			NoSuchProviderException {
		KeyGenerator kgen = KeyGenerator.getInstance(ALGORITHM);
		SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", "Crypto");
		secureRandom.setSeed(password.getBytes(charset));
		kgen.init(keysize, secureRandom);
		SecretKey secretKey = kgen.generateKey();
		byte[] enCodeFormat = secretKey.getEncoded();
		SecretKeySpec key = new SecretKeySpec(enCodeFormat, ALGORITHM);
		Cipher cipher = Cipher.getInstance("AES/ECB/ZeroBytePadding");// 创建密码器
		cipher.init(Cipher.DECRYPT_MODE, key);// 初始化
		byte[] result = cipher.doFinal(content);
		return result;
	}

	public static String byte2hex(byte[] src) {
		StringBuilder str = new StringBuilder();
		String stmp = StrKit.EMPTY;
		for (int i = 0; i < src.length; i++) {
			stmp = Integer.toHexString(src[i] & 0xFF);
			if (stmp.length() == 1) {
				str.append("0").append(stmp);
			} else {
				str.append(stmp);
			}
		}
		return str.toString();
	}

	public static final String DES3_KEY = "E69C41FC476549B7ADC89D00";
}
