package com.suv.libcore.secure;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5Kit {
	public static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	public static final String ALGORITHM = "MD5";

	public static String encode(String src, String charset) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance(ALGORITHM);
		md.update(src.getBytes(charset));
		byte[] tmp = md.digest();
		char[] chs = new char[16 * 2];
		int k = 0;
		for (int i = 0; i < 16; i++) {
			byte by = tmp[i];
			chs[k++] = HEX_DIGITS[by >>> 4 & 0xF];
			chs[k++] = HEX_DIGITS[by & 0xF];
		}
		return new String(chs);
	}

	public static String encode(byte[] source) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(ALGORITHM);
		md.update(source);
		byte[] tmp = md.digest();
		char[] chs = new char[16 * 2];
		int k = 0;
		for (int i = 0; i < 16; i++) {
			byte by = tmp[i];
			chs[k++] = HEX_DIGITS[by >>> 4 & 0xF];
			chs[k++] = HEX_DIGITS[by & 0xF];
		}
		return new String(chs);
	}

	public static String encode(String src) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		return encode(src, Charset.defaultCharset().name());
	}
}
