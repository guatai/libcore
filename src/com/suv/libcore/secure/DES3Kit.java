package com.suv.libcore.secure;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.suv.libcore.util.StrKit;

public final class DES3Kit {
	public static final String ALGORITHM = "DESede";

	public static byte[] encrypt(byte[] src, String strKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		SecretKey deskey = new SecretKeySpec(buildDesKey(strKey), ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, deskey);
		return cipher.doFinal(src);
	}

	public static byte[] decrypt(byte[] src, String strKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			IllegalBlockSizeException, BadPaddingException {
		SecretKey deskey = new SecretKeySpec(buildDesKey(strKey), ALGORITHM);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, deskey);
		return cipher.doFinal(src);
	}

	public static String encrypt2hex(byte[] src, String strKey) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {
		byte[] encrypt = encrypt(src, strKey);
		return byte2hex(encrypt);
	}

	public static String decrypt2hex(byte[] src, String strKey) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException {
		byte[] decrypt = decrypt(src, strKey);
		return byte2hex(decrypt);
	}

	public static String byte2hex(byte[] src) {
		StringBuilder str = new StringBuilder();
		String stmp = StrKit.EMPTY;
		for (int i = 0; i < src.length; i++) {
			stmp = Integer.toHexString(src[i] & 0xFF);
			if (stmp.length() == 1) {
				str.append("0").append(stmp);
			} else {
				str.append(stmp);
			}
		}
		return str.toString();
	}

	/**
	 * 转换成十六进制字符串
	 */
	public static String byte2hexSeparator(byte[] b) {
		String hs = StrKit.EMPTY;
		String stmp = StrKit.EMPTY;
		for (int i = 0; i < b.length; i++) {
			stmp = Integer.toHexString(b[i] & 0xFF);
			if (stmp.length() == 1) {
				hs = hs + "0" + stmp;
			} else {
				hs = hs + stmp;
			}
			if (i < b.length - 1) {
				hs = hs + ":";
			}
		}
		return hs;
	}

	/**
	 * 根据字符串生成密钥字节数组
	 * 
	 * @param strKey
	 *            密钥字符串
	 * @return
	 */
	public static byte[] buildDesKey(String strKey, String charset) throws UnsupportedEncodingException {
		byte[] key = new byte[24];
		byte[] tmp = strKey.getBytes(charset);
		if (key.length == tmp.length) {
			return tmp;
		} else if (key.length > tmp.length) {
			System.arraycopy(tmp, 0, key, 0, tmp.length);
		} else if (key.length < tmp.length) {
			System.arraycopy(tmp, 0, key, 0, key.length);
		}
		return key;
	}

	public static byte[] buildDesKey(String strKey) {
		byte[] key = new byte[24];
		byte[] tmp = strKey.getBytes();
		if (key.length == tmp.length) {
			return tmp;
		} else if (key.length > tmp.length) {
			System.arraycopy(tmp, 0, key, 0, tmp.length);
		} else if (key.length < tmp.length) {
			System.arraycopy(tmp, 0, key, 0, key.length);
		}
		return key;
	}

	public static void main(String[] args) throws Exception {
		String str = "0123456789";
		byte[] dest = encrypt(str.getBytes(), "123");
		String res = Base64.encode(dest);
		System.out.println(res);
	}
}
