package com.suv.libcore.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StrKit {
	public static final String EMPTY = "";
	public static final String UNDERLINE = "_";
	public static final String UTF_8 = "UTF-8";
	public static final String GBK = "GBK";
	public static final String GB2312 = "GB2312";
	public static final String UNKNOWN = "unknown";
	public static final String STR_SUCCESS = "success";
	public static final String STR_FAILURE = "failure";
	public static final String STR_FAIL = "fail";
	// regex

	public static final String EL_NUMERIC = "[\\+-]?[0-9]+((.)[0-9])*[0-9]*";
	public static final String EL_NUMERIC2 = "^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$";
	public static final String EL_NUM_WORD = "^[A-Za-z0-9]+$";
	// 只有字母、数字和下划线且不能以下划线开头和结尾的正则表达式
	public static final String EL_NUM_UNDERLINE_WORD = "^(?!_)(?!.*?_$)[a-zA-Z0-9_]+$";
	public static final String EL_MOBILE_PHONE = "^[1][3578]\\d{9}$";
	public static final String EL_DATE_YYYY_MM_DD_HH_MM_SS = "^(((20[0-3][0-9]-(0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|(20[0-3][0-9]-(0[2469]|11)-(0[1-9]|[12][0-9]|30))) (20|21|22|23|[0-1][0-9]):[0-5][0-9]:[0-5][0-9])$";
	public static final String EL_DATE_YYYY_MM_DD = "[0-9]{4}-[0-9]{2}-[0-9]{2}";
	public static final String EL_LEGAL_ASCII = "^\\p{ASCII}+$";
	public static final String EL_EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
	public static final String EL_URL_HTTP = "[http|https]+[://]+[0-9A-Za-z:/[-]_#[?][=][.][&]]*";
	public static final String EL_CHARS = "\\p{Cntrl}\\(\\)<>@,;:'\\\\\\\"\\.\\[\\]";
	public static final String EL_VALID_CHARS = "[^\\s" + EL_CHARS + "]";
	public static final String EL_QUOTED_USER = "(\"[^\"]*\")";
	public static final String EL_WORD = "((" + EL_VALID_CHARS + "|')+|" + EL_QUOTED_USER + ")";
	public static final String EL_REGEX = "^\\s*" + EL_WORD + "(\\." + EL_WORD + ")*$";

	// private static final String IP_DOMAIN_REGEX = "^\\[(.*)\\]$";

	private static final Pattern USER_PATTERN = Pattern.compile(EL_REGEX);
	private static final Pattern PATTERN_DATE_YYYY_MM_DD_HH_MM_SS = Pattern.compile(EL_DATE_YYYY_MM_DD_HH_MM_SS);
	private static final Pattern PATTERN_DATE_YYYY_MM_DD = Pattern.compile(EL_DATE_YYYY_MM_DD);

	// private static final Pattern
	// IP_DOMAIN_PATTERN=Pattern.compile(IP_DOMAIN_REGEX);

	public static String byteToString(byte[] data, String charset) throws UnsupportedEncodingException {
		return new String(data, charset);
	}

	/**
	 * 判断空字符串 包括空格
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str) {
		return (str == null || str.length() == 0);
	}

	/**
	 * 判断空字符串 不包括空格
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrBlank(String str) {
		return (str == null || str.trim().length() == 0);
	}

	/**
	 * 判断空字符串 包括空格
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str == null) return true;
		return str.length() == 0;
	}

	/**
	 * 判断空字符串 不包括空格
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str) {
		if (str == null) return true;
		return str.trim().length() == 0;
	}

	public static boolean isNullOrOverLen(String str, int minLen, int maxLen) {
		if (null == str) {
			return true;
		}
		final String trimStr = str.trim();
		if (trimStr.length() > maxLen || trimStr.length() < minLen) {
			return true;
		}
		return false;
	}

	public static boolean isOverLen(String str, int minLen, int maxLen) {
		if (str.length() > maxLen || str.length() < minLen) {
			return true;
		}
		return false;
	}

	public static boolean isNullOrLonger(String str, int len) {
		if (StrKit.isNullOrBlank(str)) {
			return true;
		}
		return str.length() > len;
	}

	public static boolean isLonger(String str, int len) {
		return str.length() > len;
	}

	/**
	 * 是否为数字
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		if (isNullOrBlank(str)) {
			return false;
		}
		return str.trim().matches(EL_NUMERIC);
	}

	/**
	 * 是否为整数
	 */
	public static boolean isInt(String str) {
		if (isNullOrBlank(str)) {
			return false;
		}
		return str.trim().matches("^-?\\d+$");
	}

	/**
	 * 内容是否只包含数字与字母
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumAndWord(String str) {
		if (isNullOrBlank(str)) {
			return false;
		}
		return str.matches(EL_NUM_WORD);
	}
	/**
	 * 正则验证
	 * @param str
	 * @param regexStr
	 * @return
	 */
	public static boolean regexValidate(String str, String regexStr) {
		if (isNullOrBlank(str)) return false;
		return str.matches(regexStr);
	}

	/**
	 * 正则 邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean validateEmail(String email) {
		if (isNullOrBlank(email)) {
			return false;
		}
		return email.matches(EL_EMAIL);
	}

	/**
	 * 正则匹配 日期 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param strDate
	 * @return
	 */
	public static boolean validateDateYMDHMS(String strDate) {
		if (StrKit.isNullOrBlank(strDate)) {
			return false;
		}
		Matcher matcher = PATTERN_DATE_YYYY_MM_DD_HH_MM_SS.matcher(strDate);
		return matcher.matches();
	}

	/**
	 * 正则匹配 日期 yyyy-MM-dd
	 * 
	 * @param strDate
	 * @return
	 */
	public static boolean validateDateYMD(String strDate) {
		if (StrKit.isNullOrBlank(strDate)) {
			return false;
		}
		Matcher matcher = PATTERN_DATE_YYYY_MM_DD.matcher(strDate);
		return matcher.matches();
	}

	public static boolean validateUser(String user) {
		return USER_PATTERN.matcher(user).matches();
	}

	/**
	 * 正则 匹配 手机号码
	 * 
	 * @param phone
	 * @return
	 */
	public static boolean validateMobilePhone(String phone) {
		if (StrKit.isNullOrBlank(phone)) {
			return false;
		}
		return phone.matches(EL_MOBILE_PHONE);
	}

	/**
	 * 正则匹配 网络地址 http:// https://
	 */
	public static boolean validateHttpUrl(String url) {
		if (StrKit.isNullOrBlank(url)) {
			return false;
		}
		Pattern pattern = Pattern.compile(EL_URL_HTTP);
		return pattern.matcher(url).matches();
	}

	/**
	 * 替换全字符空白
	 * 
	 * @param content
	 * @return
	 */
	public static String replaceFullWidthSpace(String content) {
		if (content == null) {
			return content;
		}
		return content.replace('　', ' ');
	}

	/**
	 * 字符串content 是否包含 str
	 * 
	 * @param content
	 * @param str
	 * @return
	 */
	public static boolean contains(String content, String str) {
		if (isNullOrBlank(content) || isBlank(str)) {
			return false;
		}
		return content.contains(str);
	}

	/**
	 * 格式化double类型
	 * 
	 * @param strNum
	 * @param precision
	 *            小数点位数
	 * @return
	 */
	public static String formatDouble(String strNum, int precision) {
		double num = Double.parseDouble(strNum);
		return formatDouble(num, precision);
	}

	/**
	 * 格式化double类型
	 * 
	 * @param num
	 * @param precision
	 *            小数点位数
	 * @return
	 */
	public static String formatDouble(double num, int precision) {
		StringBuilder str = new StringBuilder("0.");
		for (int i = 0; i < precision; i++) {
			str.append("0");
		}
		DecimalFormat df = new DecimalFormat(str.toString());
		return df.format(num);
	}

	/**
	 * 功能：将半角的符号转换成全角符号.(即英文字符转中文字符)
	 * 
	 * @param str
	 * @return
	 */
	public static String changeToFull(String str) {
		if (StrKit.isNullOrBlank(str)) {
			return str;
		}
		String source = "1234567890!@#$%^&*()abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_=+\\|[];:'\",<.>/?";
		char[] decode = { '１', '２', '３', '４', '５', '６', '７', '８', '９', '０', '！', '＠', '＃', '＄', '％', '︿', '＆', '＊', '（', '）', 'ａ', 'ｂ', 'ｃ', 'ｄ',
				'ｅ', 'ｆ', 'ｇ', 'ｈ', 'ｉ', 'ｊ', 'ｋ', 'ｌ', 'ｍ', 'ｎ', 'ｏ', 'ｐ', 'ｑ', 'ｒ', 'ｓ', 'ｔ', 'ｕ', 'ｖ', 'ｗ', 'ｘ', 'ｙ', 'ｚ', 'Ａ', 'Ｂ', 'Ｃ', 'Ｄ',
				'Ｅ', 'Ｆ', 'Ｇ', 'Ｈ', 'Ｉ', 'Ｊ', 'Ｋ', 'Ｌ', 'Ｍ', 'Ｎ', 'Ｏ', 'Ｐ', 'Ｑ', 'Ｒ', 'Ｓ', 'Ｔ', 'Ｕ', 'Ｖ', 'Ｗ', 'Ｘ', 'Ｙ', 'Ｚ', '－', '＿', '＝', '＋',
				'＼', '｜', '【', '】', '；', '：', '\'', '\"', '，', '〈', '。', '〉', '／', '？' };
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < str.length(); i++) {
			int pos = source.indexOf(str.charAt(i));
			if (pos != -1) {
				ret.append(decode[pos]);
			} else {
				ret.append(str.charAt(i));
			}
		}
		return ret.toString();
	}

	/**
	 * 转换unicode编码为String
	 * 
	 * @param str
	 * @return
	 */
	public static String unicodeToString(String str) {
		if (StrKit.isNullOrBlank(str)) return str;
		Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
		Matcher matcher = pattern.matcher(str);
		char ch;
		while (matcher.find()) {
			ch = (char) Integer.parseInt(matcher.group(2), 16);
			str = str.replace(matcher.group(1), String.valueOf(ch));
		}
		return str;
	}
}
