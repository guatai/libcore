package com.suv.libcore.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class FileKit {
	public static void debug(String filePath, String info, String charset) throws IOException {
		File file = new File(filePath);
		if (!file.exists()) {
			File folder = file.getParentFile();
			if (!folder.exists()) {
				folder.mkdirs();
			}
			file.createNewFile();
		}
		FileOutputStream fos = new FileOutputStream(file, true);
		fos.write(info.getBytes(charset));
		fos.close();
	}

	public static byte[] streamToByte(InputStream is) throws IOException {
		byte[] buffer = new byte[4096];
		int len = -1;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while ((len = is.read(buffer)) > 0) {
			baos.write(buffer, 0, len);
		}
		baos.close();
		return baos.toByteArray();
	}

	public static String streamToString(InputStream is, String charset) throws IOException {
		byte[] data = streamToByte(is);
		return StrKit.byteToString(data, charset);
	}
}
