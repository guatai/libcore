package com.suv.libcore.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public final class HttpKit {
	public static final String USERAGENT_CHROME = "";

	public static HttpURLConnection createDefaultHttpConn(String url) throws MalformedURLException, IOException {
		HttpURLConnection httpConn = (HttpURLConnection) new URL(url).openConnection();
		httpConn.setConnectTimeout(10 * 1000);
		httpConn.setReadTimeout(10 * 1000);
		httpConn.setRequestProperty("user-agent", USERAGENT_CHROME);
		return httpConn;
	}

	public static byte[] inputstreamToBytes(InputStream is, boolean closeStream) throws IOException {
		byte[] buffer = new byte[4096];
		int len = -1;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		while ((len = is.read(buffer)) > 0) {
			baos.write(buffer, 0, len);
		}
		if (closeStream) {
			is.close();
		}
		baos.close();
		return baos.toByteArray();
	}
}
