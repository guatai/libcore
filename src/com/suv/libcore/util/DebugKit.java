package com.suv.libcore.util;

public final class DebugKit {
	public static void print(Object msg) {
		System.out.println(msg);
	}

	public static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	public static void printWithTime(String msg) {
		System.out.println(String.format("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), msg));
	}

	public static void printWithTime(String msg, Object... args) {
		String info = String.format(msg, args);
		System.out.println(String.format("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), info));
	}
}
