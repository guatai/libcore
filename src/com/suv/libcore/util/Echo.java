package com.suv.libcore.util;

import java.util.Collection;
import java.util.Map;

public final class Echo {
	public static <T> void print(T content) {
		System.out.println(content);
	}

	public static void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	public static void printBegin() {
		System.out.println("begin...");
	}

	public static void printEnd() {
		System.out.println("end...");
	}

	public static void printEndWithTime() {
		System.out.println(String.format("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), "end..."));
	}

	public static void printBeginWithTime() {
		print("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), "begin...");
	}

	public static <T> void printWithTime(T content) {
		print("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), content);
	}

	public static void printWithTime(String msg, Object... args) {
		System.out.println(String.format("[%s] %s", DateTimeKit.format(DateTimeKit.FMT_YYYY_MM_DD_HH_MM_SS_SSS), String.format(msg, args)));
	}

	public static <T extends Collection<?>> void print(T collection) {
		for (Object item : collection) {
			System.out.println(item);
		}
	}

	public static <T extends Map<?, ?>> void print(T map) {
		print(map, "->");
	}

	/**
	 * 
	 * @param map
	 * @param splitStr
	 */
	public static <T extends Map<?, ?>> void print(T map, String splitStr) {
		for (Object key : map.keySet()) {
			System.out.println(key + splitStr + map.get(key));
		}
	}
}
