package com.suv.libcore.util;

public final class ConvertKit {
	public static byte toByte(String arg) {
		if (StrKit.isNumeric(arg)) {
			return Byte.parseByte(arg.trim());
		}
		return 0;
	}

	public static short toShort(String arg) {
		if (StrKit.isNumeric(arg)) {
			return Short.parseShort(arg);
		}
		return 0;
	}

	public static int toInt(String arg) {
		if (StrKit.isNumeric(arg)) {
			return Integer.parseInt(arg.trim());
		}
		return 0;
	}

	public static double toDouble(String arg) {
		if (StrKit.isNumeric(arg)) {
			return Double.parseDouble(arg.trim());
		}
		return 0;
	}

}
