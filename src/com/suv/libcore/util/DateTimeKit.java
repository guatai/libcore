package com.suv.libcore.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.suv.libcore.bean.SimpleMessage;

public final class DateTimeKit {
	public static final String FMT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String FMT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String FMT_YYYY_MM_DD_HH_MM_SS_SSS = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String FMT_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
	public static final String STR_DATE_EPOCH = "1970-01-01 00:00:00";
	public static final String STR_DATE_2079 = "2079-12-31 23:59:59";
	/**
	 * 新纪元 uninx
	 */
	public static final Date DATE_EPOCH;
	public static final Date DATE_2079;

	static {
		Date tmpEpoch = null;
		Date tmp2079 = null;
		try {
			tmpEpoch = parse(STR_DATE_EPOCH, FMT_YYYY_MM_DD_HH_MM_SS);
			tmp2079 = parse(STR_DATE_2079, FMT_YYYY_MM_DD_HH_MM_SS);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DATE_EPOCH = tmpEpoch;
		DATE_2079 = tmp2079;
	}

	public static Date getNow() {
		return Calendar.getInstance().getTime();
	}

	public static Timestamp getNowTimestamp() {
		return new Timestamp(getNow().getTime());
	}

	public static Date parseSafe(String date, String fmt, SimpleMessage<String> sm) {
		if (StrKit.isNullOrBlank(date)) {
			sm.setRespStatus(false);
			sm.setRespMsg(new NullPointerException().getMessage());
			return null;
		}
		final SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		Date result = null;
		try {
			result = sdf.parse(date);
			sm.setRespStatus(true);
		} catch (ParseException e) {
			sm.setRespStatus(false);
			sm.setRespMsg(e.getMessage());
		}
		return result;
	}

	public static Date parseSafe(String strDate, String fmt) {
		if (StrKit.isNullOrBlank(strDate)) {
			return null;
		}
		final SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		Date date = null;
		try {
			date = sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static String format(Date date, String fmt) {
		final SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.format(date);
	}

	public static String format() {
		return format(getNow(), FMT_YYYY_MM_DD_HH_MM_SS);
	}

	public static String format(String fmt) {
		return format(getNow(), fmt);
	}

	public static String format(Timestamp timestamp, String fmt) {
		return format(new Date(timestamp.getTime()), fmt);
	}

	public static Date parse(String strDate, String fmt) throws ParseException {
		final SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		return sdf.parse(strDate);
	}

	public static Timestamp convertTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}
}
