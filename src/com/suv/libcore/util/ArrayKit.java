package com.suv.libcore.util;

public final class ArrayKit {
	public static <T> boolean isNullOrEmpty(T[] srcArray) {
		return (srcArray == null || srcArray.length == 0);
	}
}
