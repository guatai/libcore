package com.suv.libcore.test;

public final class Constants {
	public static final String URL_BAIDU="http://www.baidu.com";
	public static final String URL_MOP="http://www.mop.com";
	public static final String URL_CSDN="http://www.csdn.com";
	public static final String URL_51CTO="http://www.51cto.com";
	public static final String URL_WEB_TEST_BASE="http://localhost:8080";
}
