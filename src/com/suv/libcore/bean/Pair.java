package com.suv.libcore.bean;

import java.io.Serializable;

/**
 * 
 * @author blue 简单双数据结构
 */
public final class Pair<FT, ST> implements Serializable {
	private FT first;
	private ST second;

	private static final long serialVersionUID = 1819847848724085578L;

	public FT getFirst() {
		return first;
	}

	public void setFirst(FT first) {
		this.first = first;
	}

	public ST getSecond() {
		return second;
	}

	public void setSecond(ST second) {
		this.second = second;
	}

	public Pair(FT first, ST second) {
		super();
		this.first = first;
		this.second = second;
	}

	public Pair() {
		super();
	}

}
