package com.suv.libcore.bean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.suv.libcore.util.StrKit;

@XmlRootElement(name = "SimpleMessage")
// 标注类名为XML根节点
@XmlAccessorType(XmlAccessType.FIELD)
// 表示将所有域作为XML节点
public final class SimpleMessage<T> implements Serializable {
	public static final String KEY_RESP_STATUS = "respStatus";
	public static final String KEY_RESP_CODE = "respCode";
	public static final String KEY_RESP_MSG = "respMsg";
	private static final long serialVersionUID = -2964741319500855392L;
	private boolean respStatus = false;
	private String respCode;
	private T respMsg;

	public SimpleMessage() {
		super();
	}

	public SimpleMessage(boolean respStatus, String respCode, T respMsg) {
		super();
		this.respStatus = respStatus;
		this.respCode = respCode;
		this.respMsg = respMsg;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public T getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(T respMsg) {
		this.respMsg = respMsg;
	}

	public boolean isRespStatus() {
		return respStatus;
	}

	public void setRespStatus(boolean respStatus) {
		this.respStatus = respStatus;
	}

	@Override
	public String toString() {
		return "简易信息 [respCode=" + respCode + ", respMsg=" + respMsg + ", respStatus=" + respStatus + "]";
	}

	public static SimpleMessage<String> createSuccessMessage(String successMessage) {
		return new SimpleMessage<String>(true, StrKit.STR_SUCCESS, successMessage);
	}

	public static SimpleMessage<String> createSuccessMessage() {
		return createSuccessMessage(StrKit.EMPTY);
	}

	public static SimpleMessage<String> createFailureMessage(String failureMessage) {
		return new SimpleMessage<String>(false, StrKit.STR_FAILURE, failureMessage);
	}

	public static SimpleMessage<String> createFailureMessage() {
		return createFailureMessage(StrKit.EMPTY);
	}
}
